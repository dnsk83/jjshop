﻿using Android.Content;
using JJShop.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(LightEntryRenderer))]
namespace JJShop.Droid.Renderers
{
    public class LightEntryRenderer : EntryRenderer
    {
        public LightEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (this.Control == null) return;
            Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Control.SetPadding(8, 8, 8, 8);
            Control.Gravity = Android.Views.GravityFlags.CenterVertical;
        }
    }
}