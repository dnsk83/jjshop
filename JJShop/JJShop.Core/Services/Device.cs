﻿using System;
namespace JJShop.Core.Services
{
    public static class Device
    {
        public const string iOS = "iOS";
        public const string Android = "Android";
        public const string Unknown = "Unknown";

        public static string RuntimePlatform { get; set; }
    }
}
