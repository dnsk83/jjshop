﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        public IMvxCommand GoToCartCommand { get; private set; }

        public LoginViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            GoToCartCommand = new MvxCommand(GoToCartCommandExecute);
        }

        private void GoToCartCommandExecute()
        {
            _navigationService.Close(this);
        }
    }
}
