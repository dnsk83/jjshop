﻿using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class ProductsGroupViewModel : MvxViewModel
    {
        public string Name { get; set; }
        public MvxObservableCollection<ProductViewModel> Products { get; set; }

        public ProductsGroupViewModel()
        {
            Products = new MvxObservableCollection<ProductViewModel>();
        }
    }
}
