﻿using System;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class ProductViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public string Name { get; set; }
        public decimal Price { get; set; }
        public string PriceFormatted => $"${Price:0.00}";

        public IMvxCommand GotoAccessoriesCommand { get; private set; }

        public ProductViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            GotoAccessoriesCommand = new MvxCommand(GotoAccessoriesCommandExecute);
        }

        private void GotoAccessoriesCommandExecute()
        {
            _navigationService.Navigate<AccessoriesViewModel>();
        }
    }
}
