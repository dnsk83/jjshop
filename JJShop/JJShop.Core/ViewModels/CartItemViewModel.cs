﻿using JJShop.Core.Models;
using MvvmCross.Commands;
using PropertyChanged;
using System;
using System.Globalization;
using System.Windows.Input;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class CartItemViewModel
    {
        private CartItemModel _Model;
        private CartViewModel _CartViewModel;

        public string Name => _Model.Name;
        public string Details => _Model.Details;
        public string Price => _Model.Price.ToString("C", CultureInfo.GetCultureInfo("en-US"));
        public string Count => string.Format("{0} In Stock", _Model.Count);
        public string LineNotes => _Model.LineNotes;
        public bool LineNotesVisible => !string.IsNullOrEmpty(LineNotes);

        public IMvxCommand DeleteItemCommand { get; private set; }

        public CartItemViewModel(CartItemModel model, CartViewModel listViewModel = null)
        {
            _Model = model;
            _CartViewModel = listViewModel;
            DeleteItemCommand = new MvxCommand(DeleteItemCommandExecute);
        }

        private void DeleteItemCommandExecute()
        {
            if(_CartViewModel != null)
            {
                _CartViewModel.Items.Remove(this);
            }
        }
    }

}
