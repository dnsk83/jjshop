﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class AccountViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        public IMvxCommand GoToListsCommand { get; private set; }
        public IMvxCommand ShowDemoCommand { get; private set; }

        public AccountViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            GoToListsCommand = new MvxCommand(GoToListCommandExecute);
            ShowDemoCommand = new MvxCommand(ShowDemoCommandExecute);
        }

        private void GoToListCommandExecute()
        {
            _navigationService.Navigate<ListsViewModel>();
        }
        
        private void ShowDemoCommandExecute()
        {
            _navigationService.Navigate<DemoViewModel>();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
        }
    }
}
