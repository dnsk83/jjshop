﻿using JJShop.Core.Models;
using MvvmCross.ViewModels;
using PropertyChanged;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ListsViewModel : MvxViewModel
    {
        public ObservableCollection<ListsListItemViewModel> Items { get; private set; }

        public override async Task Initialize()
        {
            await base.Initialize();
            Items = new ObservableCollection<ListsListItemViewModel>();
            LoadFakeItems();
        }

        private void LoadFakeItems()
        {
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Artur Vander Voort List", Details = "Random description goes here and there", Price = 180000M, Count = 5 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Red Flags Lists", Details = "A few other things in this order", Price = 24413.39M, Count = 11 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "There is no character limit to list name", Details = "Character limit is unlimited for description", Price = 1000.39M, Count = 1100 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other list", Details = "Short description", Price = 4000M, Count = 1432 }, this));

            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other2 2list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other3 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other4 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other5 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other7 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other6 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other8 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other9 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other0 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
            Items.Add(new ListsListItemViewModel(new ListsListItemModel() { Name = "Other1 list", Details = "Short description", Price = 4000M, Count = 1432 }, this));
        }
    }
}
