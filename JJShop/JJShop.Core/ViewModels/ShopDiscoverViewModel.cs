﻿using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using PropertyChanged;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ShopDiscoverViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public string RotatedImageText => "Restricted Workflow";
        public string RotatedImageDetails => "Clearance Inventory Sale";
        public MvxObservableCollection<ProductsGroupViewModel> ProductGroups { get; set; }

        public ShopDiscoverViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override void Prepare()
        {
            base.Prepare();

            ProductGroups = new MvxObservableCollection<ProductsGroupViewModel>();
            LoadFakeGroups();
        }

        private void LoadFakeGroups()
        {
            FakeGroup1();
            FakeGroup2();
            FakeGroup3();
        }

        private void FakeGroup1()
        {
            var groupVM = new ProductsGroupViewModel() { Name = "First Group" };
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Plane", Price = 33 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Car", Price = 1440 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Bike", Price = 660 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "House", Price = 9900 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Garage", Price = 990 });
            ProductGroups.Add(groupVM);
        }

        private void FakeGroup2()
        {
            var groupVM = new ProductsGroupViewModel() { Name = "Another Group" };
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "A Product with Very Long Name", Price = 10 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Duck", Price = 14 });
            ProductGroups.Add(groupVM);
        }

        private void FakeGroup3()
        {
            var groupVM = new ProductsGroupViewModel() { Name = "One more Group" };
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Blackberry", Price = 500 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "iOS", Price = 700 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Android", Price = 300 });
            groupVM.Products.Add(new ProductViewModel(_navigationService) { Name = "Windows", Price = 400 });
            ProductGroups.Add(groupVM);
        }
    }
}
