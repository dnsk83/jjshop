﻿using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class DemoViewModel : MvxViewModel
    {
        private IMvxNavigationService _navigationService;

        public string DemoText => "Another Page";

        public DemoViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void NavigateBack()
        {
            _navigationService.Close(this);
        }
    }
}
