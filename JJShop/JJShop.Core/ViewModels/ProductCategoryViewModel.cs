﻿using JJShop.Core.Models;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Text;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ProductCategoryViewModel : MvxViewModel
    {
        private ProductCategoryModel _Model;
        private readonly IMvxNavigationService _navigationService;

        public string Name => _Model.Name;
        public bool Enabled { get; set; }
        public IMvxCommand GotoCategoryCommand { get; private set; }

        public ProductCategoryViewModel(ProductCategoryModel model, IMvxNavigationService navigationService) 
        {
            _navigationService = navigationService;
            _Model = model;
            GotoCategoryCommand = new MvxCommand(GotoCategoryCommandExecute);
        }

        private void GotoCategoryCommandExecute()
        {
            _navigationService.Navigate<DemoViewModel>();
        }
    }
}
