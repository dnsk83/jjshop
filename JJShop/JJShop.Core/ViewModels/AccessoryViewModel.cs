﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public enum Availability
    {
        InStock,
        OutOfStock,
        CallForAvailability
    }

    public class AccessoryViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public string Name { get; set; }
        public string Details { get; set; }
        public string Value => "$Value";
        public Availability Availability { get; set; }
        public IMvxCommand GotoDemoCommand { get; private set; }

        public AccessoryViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            GotoDemoCommand = new MvxCommand(GotoDemoCommandExecute);
        }

        private void GotoDemoCommandExecute()
        {
            _navigationService.Navigate<DemoViewModel>();
        }
    }
}
