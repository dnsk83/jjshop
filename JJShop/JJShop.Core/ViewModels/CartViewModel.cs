﻿using JJShop.Core.Models;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using PropertyChanged;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class CartViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public ObservableCollection<CartItemViewModel> Items { get; private set; }

        public CartViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            Items = new ObservableCollection<CartItemViewModel>();
            LoadFakeItems();
        }

        private void LoadFakeItems()
        {
            Items.Add(new CartItemViewModel(new CartItemModel() { Name = "HTC Vive Replacement", Details = "9116456423459846", Price = 99.99M, Count = 999 }, this));
            Items.Add(new CartItemViewModel(new CartItemModel() { Name = "Product Name May Be Two Lines Long", Details = "SKU or UPC", Price = 0M, Count = 999, LineNotes = "Please deliver this to the loading dock 400 in the south-wing" }, this));
            Items.Add(new CartItemViewModel(new CartItemModel() { Name = "Product Name", Details = "SKU or UPC", Price = 0M, Count = 999, LineNotes = "Please deliver this to the loading dock 400 in the south-wing" }, this));
        }
    }
}
