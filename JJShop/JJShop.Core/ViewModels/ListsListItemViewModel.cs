﻿using JJShop.Core.Models;
using MvvmCross.Commands;
using PropertyChanged;
using System;
using System.Globalization;
using System.Windows.Input;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ListsListItemViewModel
    {
        private ListsListItemModel _Model;
        private ListsViewModel _ListViewModel;

        public string Name => _Model.Name;
        public string Details => _Model.Details;
        public string Price => _Model.Price.ToString("C", CultureInfo.GetCultureInfo("en-US"));
        public string Count => string.Format("{0} Products", _Model.Count);

        public IMvxCommand DeleteItemCommand { get; private set; }

        public ListsListItemViewModel(ListsListItemModel model, ListsViewModel listViewModel = null)
        {
            _Model = model;
            _ListViewModel = listViewModel;
            DeleteItemCommand = new MvxCommand(DeleteItemCommandExecute);
        }

        private void DeleteItemCommandExecute()
        {
            if(_ListViewModel != null)
            {
                _ListViewModel.Items.Remove(this);
            }
        }
    }

}
