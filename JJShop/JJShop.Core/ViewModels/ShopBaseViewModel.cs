﻿using System;
using MvvmCross;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class ShopBaseViewModel : MvxViewModel
    {
        public ShopDiscoverViewModel ShopDiscoverVM { get; set; }
        public ShopProductCategoriesViewModel ShopProductsVM { get; set; }

        public override void Prepare()
        {
            var sdvm = Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<ShopDiscoverViewModel>.GetDefaultRequest(), null) as ShopDiscoverViewModel;
            ShopDiscoverVM = sdvm;
            var spvm = Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<ShopProductCategoriesViewModel>.GetDefaultRequest(), null) as ShopProductCategoriesViewModel;
            ShopProductsVM = spvm;
        }
    }
}
