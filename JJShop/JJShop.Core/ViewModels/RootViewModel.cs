﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;
using MvvmCross.Navigation;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using JJShop.Core.Services;

namespace JJShop.Core.ViewModels
{
    public class RootViewModel : MvxViewModel
    {
        private IMvxNavigationService _navigationService;
        public IMvxAsyncCommand ShowInitialViewModelsCommand { get; private set; }

        public RootViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            ShowInitialViewModelsCommand = new MvxAsyncCommand(ShowInitialViewModels);
        }

        private async Task ShowInitialViewModels()
        {
            var tasks = new List<Task>();
            if (Device.RuntimePlatform == Device.Android)
            {
                tasks.Add(_navigationService.Navigate<ShopTabbedViewModel>());
            }
            if (Device.RuntimePlatform == Device.iOS)
            {
                tasks.Add(_navigationService.Navigate<ShopSegmentedViewModel>());
            }
            tasks.Add(_navigationService.Navigate<SearchViewModel>());
            tasks.Add(_navigationService.Navigate<AccountViewModel>());
            tasks.Add(_navigationService.Navigate<CartViewModel>());
            await Task.WhenAll(tasks);
        }

    }
}
