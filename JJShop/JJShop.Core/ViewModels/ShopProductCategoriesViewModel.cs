﻿using JJShop.Core.Models;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Text;

namespace JJShop.Core.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ShopProductCategoriesViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        public MvxObservableCollection<ProductCategoryViewModel> Categories { get; set; }

        public ShopProductCategoriesViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override void Prepare()
        {
            base.Prepare();

            Categories = new MvxObservableCollection<ProductCategoryViewModel>();
            LoadFakeCategories();
        }

        private void LoadFakeCategories()
        {
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "Bearings" }, _navigationService) { Enabled = false });
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "Stuff" }, _navigationService) { Enabled = false });
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "Clearance Items" }, _navigationService) { Enabled = false });
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "MFG Products" }, _navigationService) { Enabled = false });
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "Configure Your Product" }, _navigationService) { Enabled = false });
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "Office" }, _navigationService) { Enabled = true });
            Categories.Add(new ProductCategoryViewModel(new ProductCategoryModel() { Name = "Accessories" }, _navigationService) { Enabled = false });
        }
    }
}
