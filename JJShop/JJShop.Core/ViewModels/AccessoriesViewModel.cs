﻿using System;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace JJShop.Core.ViewModels
{
    public class AccessoriesViewModel : MvxViewModel
    {
        private IMvxNavigationService _navigationService;

        public MvxObservableCollection<AccessoryViewModel> Accessories { get; set; }
        public IMvxCommand GoBackCommand { get; private set; }
        public IMvxCommand GotoDemoCommand { get; private set; }

        public AccessoriesViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            GoBackCommand = new MvxCommand(GoBackCommandExecute);
            GotoDemoCommand = new MvxCommand(GotoDemoCommandExecute);
            Accessories = new MvxObservableCollection<AccessoryViewModel>();
            LoadFakeAccessories();
        }

        private void GotoDemoCommandExecute()
        {
            _navigationService.Navigate<DemoViewModel>();
        }

        private void LoadFakeAccessories()
        {
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.OutOfStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.CallForAvailability });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.OutOfStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
            Accessories.Add(new AccessoryViewModel(_navigationService) { Name = "Product Name", Details = "SKU or UPC", Availability = Availability.InStock });
        }

        private void GoBackCommandExecute()
        {
            _navigationService.Close(this);
        }
    }
}
