﻿using PropertyChanged;

namespace JJShop.Core.Models
{
    [AddINotifyPropertyChangedInterface]
    public class CartItemModel
    {
        public string Name { get; set; }
        public string Details { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
        public string LineNotes { get; set; }
    }
}
