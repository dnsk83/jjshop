﻿using PropertyChanged;

namespace JJShop.Core.Models
{
    [AddINotifyPropertyChangedInterface]
    public class ListsListItemModel
    {
        public string Name { get; set; }
        public string Details { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
    }
}
