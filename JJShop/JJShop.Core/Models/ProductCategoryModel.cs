﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Text;

namespace JJShop.Core.Models
{
    [AddINotifyPropertyChangedInterface]
    public class ProductCategoryModel
    {
        public string Name { get; set; }
    }
}
