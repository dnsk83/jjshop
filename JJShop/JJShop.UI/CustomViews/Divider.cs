﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace JJShop.UI.CustomViews
{
	public class Divider : BoxView
	{
		public Divider ()
		{
            HeightRequest = 1;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            BackgroundColor = (Color)Application.Current.Resources["Divider"];
		}
	}
}