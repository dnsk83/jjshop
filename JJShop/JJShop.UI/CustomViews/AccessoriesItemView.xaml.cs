﻿using JJShop.Core.ViewModels;
using JJShop.UI.ValueConverters;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AccessoriesItemView : MvxContentView<AccessoryViewModel>
	{
        public static readonly BindableProperty NameProperty = BindableProperty.Create(nameof(Name), typeof(string), typeof(AccessoriesItemView), default(string), propertyChanged: NamePropertyChanged);
        public string Name
        {
            get => (string)GetValue(NameProperty);
            set
            {
                if (Name != value)
                {
                    SetValue(NameProperty, value);
                }
            }
        }

        public static readonly BindableProperty DetailsProperty = BindableProperty.Create(nameof(Details), typeof(string), typeof(AccessoriesItemView), default(string), propertyChanged: DetailsPropertyChanged);
        public string Details
        {
            get => (string)GetValue(DetailsProperty);
            set
            {
                if (Details != value)
                {
                    SetValue(DetailsProperty, value);
                }
            }
        }

        public static readonly BindableProperty ValueProperty = BindableProperty.Create(nameof(Value), typeof(string), typeof(AccessoriesItemView), default(string), propertyChanged: ValuePropertyChanged);
        public string Value
        {
            get => (string)GetValue(ValueProperty);
            set
            {
                if (Value != value)
                {
                    SetValue(ValueProperty, value);
                }
            }
        }

        public static readonly BindableProperty AvailabilityProperty = BindableProperty.Create(nameof(Availability), typeof(Availability), typeof(AccessoriesItemView), default(Availability), propertyChanged: AvailabilityPropertyChanged);
        public Availability Availability
        {
            get => (Availability)GetValue(AvailabilityProperty);
            set
            {
                if (Availability != value)
                {
                    SetValue(NameProperty, value);
                }
            }
        }

        private static void NamePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as AccessoriesItemView;
            if (control == null) return;
            control.Name = (string)newValue;
        }

        private static void DetailsPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as AccessoriesItemView;
            if (control == null) return;
            control.Details = (string)newValue;
        }

        private static void ValuePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as AccessoriesItemView;
            if (control == null) return;
            control.Value = (string)newValue;
        }

        private static void AvailabilityPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as AccessoriesItemView;
            if (control == null) return;
            control.Availability = (Availability)newValue;
        }

        public AccessoriesItemView ()
		{
			InitializeComponent ();
		}

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (BindingContext != null)
            {
                txtName.Text = Name;
                txtDetails.Text = Details;
                txtValue.Text = Value;
                txtAvailability.Text = AvailabilityToString(Availability);
                txtAvailability.TextColor = AvailabilityToColor(Availability);
            }
        }

        private string AvailabilityToString(Availability availability)
        {
            switch (availability)
            {
                case Availability.InStock:
                    return "In Stock";
                case Availability.OutOfStock:
                    return "Out Of Stock";
                case Availability.CallForAvailability:
                    return "Call For Availability";
                default:
                    break;
            }
            return "N/A";
        }

        private Color AvailabilityToColor(Availability availability)
        {
            var red = Color.FromHex("FF543E");
            var defaultGray = Color.FromHex("999999");
            if (availability == Availability.OutOfStock)
            {
                return red;
            }
            return defaultGray;
        }
    }
}