﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CompanyAddressView : TapableView
    {
        public string Text
        {
            get => txtCaption.Text;
            set => txtCaption.Text = value;
        }

        public ImageSource ImageSource
        {
            get => imgImage.Source;
            set => imgImage.Source = value;
        }

        public CompanyAddressView ()
		{
			InitializeComponent ();
		}
	}
}