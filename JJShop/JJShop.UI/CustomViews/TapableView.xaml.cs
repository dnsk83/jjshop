﻿using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TapableView : MvxContentView
    {
        public event EventHandler Clicked;

        double initialOpacity;

        public static readonly BindableProperty CommandProperty = BindableProperty.Create(
            nameof(Command),
            typeof(ICommand),
            typeof(TapableView),
            null);

        public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        public TapableView()
        {
            InitializeComponent();

            var mainTgr = new TapGestureRecognizer();
            mainTgr.Tapped += MainTgr_TappedAsync;
            GestureRecognizers.Add(mainTgr);

            initialOpacity = this.Opacity;

            VerticalOptions = LayoutOptions.CenterAndExpand;
            HorizontalOptions = LayoutOptions.CenterAndExpand;
        }

        async void MainTgr_TappedAsync(object sender, EventArgs e)
        {
            await this.FadeTo(0, 40);
            Command?.Execute(null);
            Clicked?.Invoke(this, new EventArgs());
            await this.FadeTo(initialOpacity, 200);
        }
    }
}