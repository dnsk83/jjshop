﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountActionListItem : TapableView
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(AccountActionListItem), default(string), propertyChanged: TextPropertyChanged);

        private static void TextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as AccountActionListItem;
            if (control == null) return;
            control.Text = (string)newValue;
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set
            {
                if (Text != value)
                {
                    SetValue(TextProperty, value);
                }
            }
        }

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(AccountActionListItem), default(Color), propertyChanged: TextColorPropertyChanged);

        public Color TextColor
        {
            get => (Color)GetValue(TextColorProperty);
            set
            {
                if (TextColor != value)
                {
                    SetValue(TextColorProperty, value);
                }
            }
        }

        private static void TextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as AccountActionListItem;
            if (control == null) return;
            control.TextColor = (Color)newValue;
        }

        public ImageSource ImageSource
        {
            get => imgImage.Source;
            set => imgImage.Source = value;
        }

        public AccountActionListItem()
        {
            InitializeComponent();

            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            HeightRequest = 48;
        }

        protected override void OnBindingContextChanged()
        {
            if (BindingContext!=null)
            {
                txtText.Text = Text;
                txtText.TextColor = TextColor;
            }
            base.OnBindingContextChanged();
        }
    }
}