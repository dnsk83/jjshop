﻿using JJShop.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static JJShop.Resources.Constants;

namespace JJShop.UI.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListsListItemView : ContentView
    {
        #region Bindable
        public static readonly BindableProperty NameProperty = BindableProperty.Create(nameof(Name), typeof(string), typeof(ListsListItemView), default(string), propertyChanged: NamePropertyChanged);
        public string Name
        {
            get => (string)GetValue(NameProperty);
            set
            {
                if (Name != value)
                {
                    SetValue(NameProperty, value);
                }
            }
        }

        public static readonly BindableProperty DetailsProperty = BindableProperty.Create(nameof(Details), typeof(string), typeof(ListsListItemView), default(string), propertyChanged: DetailsPropertyChanged);
        public string Details
        {
            get => (string)GetValue(DetailsProperty);
            set
            {
                if (Details != value)
                {
                    SetValue(DetailsProperty, value);
                }
            }
        }

        public static readonly BindableProperty TotalPriceProperty = BindableProperty.Create(nameof(TotalPrice), typeof(string), typeof(ListsListItemView), default(string), propertyChanged: TotalPricePropertyChanged);
        public string TotalPrice
        {
            get => (string)GetValue(TotalPriceProperty);
            set
            {
                if (TotalPrice != value)
                {
                    SetValue(TotalPriceProperty, value);
                }
            }
        }

        public static readonly BindableProperty CountProperty = BindableProperty.Create(nameof(Count), typeof(string), typeof(ListsListItemView), default(string), propertyChanged: CountPropertyChanged);
        public string Count
        {
            get => (string)GetValue(CountProperty);
            set
            {
                if (Count != value)
                {
                    SetValue(NameProperty, value);
                }
            }
        }

        private static void NamePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as ListsListItemView;
            if (control == null) return;
            control.Name = (string)newValue;
        }

        private static void DetailsPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as ListsListItemView;
            if (control == null) return;
            control.Details = (string)newValue;
        }

        private static void TotalPricePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as ListsListItemView;
            if (control == null) return;
            control.TotalPrice = (string)newValue;
        }

        private static void CountPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as ListsListItemView;
            if (control == null) return;
            control.Count = (string)newValue;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (BindingContext != null)
            {
                txtName.Text = Name;
                txtDetails.Text = Details;
                txtTotalPrice.Text = TotalPrice;
                txtCount.Text = Count;
                btnDelete.Command = ((ListsListItemViewModel)BindingContext).DeleteItemCommand;
                btnDelete.Clicked += Btn_Clicked;
                btnMore.Clicked += Btn_Clicked; 
                btnCart.Clicked += Btn_Clicked;
            }
            if (_IsSwiped)
            {
                Unswipe(false);
            }
        }

        #endregion Bindable

        private uint _AnimationLengthMillis = 150;
		private bool _IsSwiped;

        public ListsListItemView()
        {
            InitializeComponent();
            Content.BindingContext = this;
            CreateGestureRecognizersForRoot();

            MessagingCenter.Subscribe<ListsListItemView>(this, Strings.ImSwiped, OnSwipedMessageReceived);
            MessagingCenter.Subscribe<ListsListItemView>(this, Strings.ImTapped, OnSwipedMessageReceived);
        }

        private void OnSwipedMessageReceived(ListsListItemView sender)
        {
			if (sender != this)
            {
                Unswipe();
            }
        }

        private void CreateGestureRecognizersForRoot()
        {
            if (Device.RuntimePlatform!=Device.iOS)
            {
                var pgr = new PanGestureRecognizer();
                pgr.PanUpdated += Root_PanUpdated;
                vRoot.GestureRecognizers.Add(pgr);
            }

            var tgr = new TapGestureRecognizer();
            tgr.Tapped += Tgr_Tapped;
            vRoot.GestureRecognizers.Add(tgr);
        }

        private void Tgr_Tapped(object sender, EventArgs e)
        {
			if (_IsSwiped)
            {
                Unswipe();
            }
            else
            {
                NotifyAboutTap();
            }
        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            Unswipe(false);
        }

        private void NotifyAboutTap()
        {
            MessagingCenter.Send<ListsListItemView>(this, Strings.ImTapped);
        }

        public void NotifyAboutSwipe()
        {
            MessagingCenter.Send<ListsListItemView>(this, Strings.ImSwiped);
        }

        private void Root_PanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Running:
                    var onSwipeToLeft = e.TotalX < 0;
                    if (onSwipeToLeft)
                    {
                        Swipe();
                        NotifyAboutSwipe();
                    }
                    else
                    {
                        Unswipe();
                    }
                    break;
            }
        }

        public void Swipe()
        {
            vRoot.TranslateTo(-1 * vRoot.Height * 3, 0, _AnimationLengthMillis);
			_IsSwiped = true;
        }

        public void Unswipe(bool animated = true)
        {
            vRoot.TranslateTo(0, 0, animated ? _AnimationLengthMillis : 0);
			_IsSwiped = false;
        }
    }
}