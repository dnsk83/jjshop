﻿using CarouselView.FormsPlugin.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RotatedImageView : CarouselViewControl
    {
		public RotatedImageView ()
		{
			InitializeComponent ();
            DoInfiniteRotation();
        }

        private async void DoInfiniteRotation()
        {
            while (true)
            {
                var items = ItemsSource as Array;
                if (Position >= items.Length - 1)
                {
                    Position = 0;
                }
                else
                {
                    Position++;
                }
                await Task.Delay(5000);
            }
        }
    }
}