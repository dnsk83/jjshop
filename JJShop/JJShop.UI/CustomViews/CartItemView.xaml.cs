﻿using JJShop.Core.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static JJShop.Resources.Constants;

namespace JJShop.UI.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartItemView : ViewCell
    {
        #region Bindable
        public static readonly BindableProperty NameProperty = BindableProperty.Create(nameof(Name), typeof(string), typeof(CartItemView), default(string), propertyChanged: NamePropertyChanged);
        public string Name
        {
            get => (string)GetValue(NameProperty);
            set
            {
                if (Name != value)
                {
                    SetValue(NameProperty, value);
                }
            }
        }

        public static readonly BindableProperty DetailsProperty = BindableProperty.Create(nameof(Details), typeof(string), typeof(CartItemView), default(string), propertyChanged: DetailsPropertyChanged);
        public string Details
        {
            get => (string)GetValue(DetailsProperty);
            set
            {
                if (Details != value)
                {
                    SetValue(DetailsProperty, value);
                }
            }
        }

        public static readonly BindableProperty TotalPriceProperty = BindableProperty.Create(nameof(TotalPrice), typeof(string), typeof(CartItemView), default(string), propertyChanged: TotalPricePropertyChanged);
        public string TotalPrice
        {
            get => (string)GetValue(TotalPriceProperty);
            set
            {
                if (TotalPrice != value)
                {
                    SetValue(TotalPriceProperty, value);
                }
            }
        }

        public static readonly BindableProperty CountProperty = BindableProperty.Create(nameof(Count), typeof(string), typeof(CartItemView), default(string), propertyChanged: CountPropertyChanged);
        public string Count
        {
            get => (string)GetValue(CountProperty);
            set
            {
                if (Count != value)
                {
                    SetValue(NameProperty, value);
                }
            }
        }

        private static void NamePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as CartItemView;
            if (control == null) return;
            control.Name = (string)newValue;
        }

        private static void DetailsPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as CartItemView;
            if (control == null) return;
            control.Details = (string)newValue;
        }

        private static void TotalPricePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as CartItemView;
            if (control == null) return;
            control.TotalPrice = (string)newValue;
        }

        private static void CountPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as CartItemView;
            if (control == null) return;
            control.Count = (string)newValue;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (BindingContext != null)
            {
                txtName.Text = Name;
                txtDetails.Text = Details;
                txtTotalPrice.Text = TotalPrice;
                txtCount.Text = Count;
                btnDelete.Command = ((CartItemViewModel)BindingContext).DeleteItemCommand;
                btnDelete.Clicked += Btn_Clicked;
            }
        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            Unswipe(false);
        }

        #endregion Bindable

        private bool _SizeUpdated;
        private uint _AnimationLengthMillis = 150;
		private bool _IsSwiped;
        private bool _UseCustomSlidingMenuForAndroid = (bool)Application.Current.Resources["UseCustomSlidingMenuForAndroid"];

        public CartItemView()
        {
            InitializeComponent();
            AddContextActions();

            slHighestStackLayout.LayoutChanged += SlHighestStackLayout_LayoutChanged;
            MessagingCenter.Subscribe<CartItemView>(this, Strings.ImSwiped, OnSwipedMessageReceived);
            MessagingCenter.Subscribe<CartItemView>(this, Strings.ImTapped, OnSwipedMessageReceived);
        }

        private void OnSwipedMessageReceived(CartItemView sender)
        {
			if (sender != this)
            {
                Unswipe();
            }
        }

        private void AddContextActions()
        {
            if (_UseCustomSlidingMenuForAndroid && Device.RuntimePlatform != Device.iOS)
            {
                CreateGestureRecognizersForRoot();
            }
            else
            {
                var deleteMenuItem = new MenuItem();
                deleteMenuItem.IsDestructive = true;
                deleteMenuItem.Text = "Delete";
                deleteMenuItem.Clicked += DeleteMenuItem_Clicked;
                ContextActions.Add(deleteMenuItem);
            }
        }

        private void DeleteMenuItem_Clicked(object sender, EventArgs e)
        {
            ((CartItemViewModel)BindingContext).DeleteItemCommand.Execute(null);
        }

        private void CreateGestureRecognizersForRoot()
        {
            var pgr = new PanGestureRecognizer();
            pgr.PanUpdated += Root_PanUpdated;
            vRoot.GestureRecognizers.Add(pgr);

            var tgr = new TapGestureRecognizer();
            tgr.Tapped += Tgr_Tapped;
            vRoot.GestureRecognizers.Add(tgr);
        }

        private void Tgr_Tapped(object sender, EventArgs e)
        {
            if (_IsSwiped)
            {
                Unswipe();
            }
            else
            {
                NotifyAboutTap();
            }
        }

        private void NotifyAboutTap()
        {
            MessagingCenter.Send<CartItemView>(this, Strings.ImTapped);
        }

        private void NotifyAboutSwipe()
        {
            MessagingCenter.Send<CartItemView>(this, Strings.ImSwiped);
        }

        private void Root_PanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Running:
                    var isSwipedToLeft = e.TotalX < 0;
                    if (isSwipedToLeft)
                    {
                        Swipe();
                        NotifyAboutSwipe();
                    }
                    else
                    {
                        Unswipe();
                    }
                    break;
            }
        }

        private void Swipe()
        {
            vRoot.TranslateTo(-1 * 76, 0, _AnimationLengthMillis);
			_IsSwiped = true;
        }

		private void Unswipe(bool animated = true)
        {
            vRoot.TranslateTo(0, 0, animated ? _AnimationLengthMillis : 0);
            _IsSwiped = false;
        }

        private void SlHighestStackLayout_LayoutChanged(object sender, EventArgs e)
        {
            CalculateCellHeight();
        }

        private void CalculateCellHeight()
        {
			if (_SizeUpdated)
			{
				return;
			}
			double totalHeight = 0;
            foreach (var element in slHighestStackLayout.Children)
            {
				if (element.IsVisible)
				{
					totalHeight += element.Height;
				}
            }
            totalHeight += vRoot.Padding.Top + vRoot.Padding.Bottom;
			alRoot.HeightRequest = totalHeight;
			_SizeUpdated = true;
        }
    }
}