﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductsGroupView : MvxContentView<ProductsGroupViewModel>
	{
		public ProductsGroupView ()
		{
			InitializeComponent ();
		}

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            CreateProductsLayout();
        }


        private void CreateProductsLayout()
        {
            var vm = ViewModel as ProductsGroupViewModel;
            if (vm != null)
            {
                foreach (var productVM in vm.Products)
                {
                    var productView = new ProductView();
                    productView.VerticalOptions = LayoutOptions.Start;
                    productView.ViewModel = productVM;
                    SLProducts.Children.Add(productView);
                }
            }
        }
    }
}