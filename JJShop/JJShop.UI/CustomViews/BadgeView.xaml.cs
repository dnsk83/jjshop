﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BadgeView : ContentView
    {
        public string Text
        {
            get => txtText.Text;
            set => txtText.Text = value;
        }

        public BadgeView()
        {
            InitializeComponent();
        }
    }
}