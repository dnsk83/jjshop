﻿using MvvmCross.Converters;
using MvvmCross.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace JJShop.UI.ValueConverters
{
    public class BoolToColorConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return Color.FromRgb(0x51, 0x51, 0x51);
            }
            else
            {
                return Color.FromRgb(0xA8, 0xA8, 0xA8);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}
