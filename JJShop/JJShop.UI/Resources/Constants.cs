﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JJShop.Resources
{
    public static class Constants
    {
        public static class Strings
        {
            public const string ImTapped = "ImTapped";
            public const string ImSwiped = "ImSwiped";
        }
    }
}
