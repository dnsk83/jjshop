﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages.Shop
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(TabbedPosition.Tab, Title = "Shop", Icon = "ic_shop.png")]
    public partial class ShopTabbedPage : MvxTabbedPage<ShopTabbedViewModel>
    {
        public ShopTabbedPage()
        {
            InitializeComponent();
        }
    }
}