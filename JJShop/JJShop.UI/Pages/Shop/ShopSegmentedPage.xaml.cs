﻿using System;
using System.Collections.Generic;
using JJShop.Core.ViewModels;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages.Shop
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(TabbedPosition.Tab, Title = "Shop", Icon = "ic_shop.png")]
    public partial class ShopSegmentedPage : MvxContentPage<ShopSegmentedViewModel>
    {
        public ShopSegmentedPage()
        {
            InitializeComponent();
            ShowDiscover();
        }

        void Handle_DiscoverClicked(object sender, System.EventArgs e)
        {
            OnDiscoverSegmentSelected();
            ShowDiscover();
        }

        void Handle_ProductsClicked(object sender, System.EventArgs e)
        {
            OnProductsSegmentSelected();
            ShowProducts();
        }

        private void ShowDiscover()
        {
            vDiscover.IsVisible = true;
            vProducts.IsVisible = false;
        }

        private void ShowProducts()
        {
            vProducts.IsVisible = true;
            vDiscover.IsVisible = false;
        }

        private void OnDiscoverSegmentSelected()
        {
            lblDiscover.BackgroundColor = Color.White;
            lblDiscover.TextColor = (Color)Application.Current.Resources["Primary"];
            lblProducts.BackgroundColor = (Color)Application.Current.Resources["Primary"];
            lblProducts.TextColor = Color.White;
        }

        private void OnProductsSegmentSelected()
        {
            lblProducts.BackgroundColor = Color.White;
            lblProducts.TextColor = (Color)Application.Current.Resources["Primary"];
            lblDiscover.BackgroundColor = (Color)Application.Current.Resources["Primary"];
            lblDiscover.TextColor = Color.White;
        }
    }
}
