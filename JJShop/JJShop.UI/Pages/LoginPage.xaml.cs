﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : MvxContentPage<LoginViewModel>
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}
	}
}