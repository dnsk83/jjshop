﻿using JJShop.Core.ViewModels;
using JJShop.UI.CustomViews;
using MvvmCross.Forms.Views;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static JJShop.Resources.Constants;

namespace JJShop.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListsPage : MvxContentPage<ListsViewModel>
	{
		public ListsPage()
		{
			InitializeComponent();

			btnBack.Clicked += BtnBack_Clicked;

			if (Device.RuntimePlatform != Device.iOS)
			{
				var tgr = new TapGestureRecognizer();
				tgr.Tapped += (s, e) => { vMoreMenu.IsVisible = false; };
				vMoreMenu.GestureRecognizers.Add(tgr);

				btnMore.Clicked += BtnMore_Clicked_ShowMenu;
			}
			else
			{
				btnMore.Clicked += (s, e) =>
				{
					DisplayActionSheet(null, "Cancel", null, "Select 1", "Select 2");
				};
			}

            MessagingCenter.Subscribe<ListsListItemView>(this, Strings.ImTapped, OnItemTappedMessageReceived);
        }

        private void OnItemTappedMessageReceived(ListsListItemView sender)
        {
            lvItems.SelectedItem = sender;
        }

        private void BtnBack_Clicked(object sender, EventArgs e)
		{
			Navigation.PopAsync();
		}

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem == null) return;

            var selectedItem = e.SelectedItem as ListsListItemView;
            DisplayAlert(selectedItem.Name, selectedItem.Details, "Ok");

			((ListView)sender).SelectedItem = null;
		}

		private void MoreMenuItem_Clicked(object sender, EventArgs e)
		{
			vMoreMenu.IsVisible = false;
		}

		private void BtnMore_Clicked_ShowMenu(object sender, EventArgs e)
		{
			vMoreMenu.IsVisible = true;
		}
	}
}