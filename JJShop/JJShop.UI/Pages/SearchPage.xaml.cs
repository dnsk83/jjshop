﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(TabbedPosition.Tab, Title = "Search", Icon = "ic_search.png")]
    public partial class SearchPage : MvxContentPage<SearchViewModel>
    {

        public SearchPage()
        {
            InitializeComponent();

            var tgr = new TapGestureRecognizer();
            tgr.Tapped += (s, e) => { vSortMenu.IsVisible = false; };
            vSortMenu.GestureRecognizers.Add(tgr);
        }

        private void LvMain_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;

            ((ListView)sender).SelectedItem = null;
        }

        private void Sort_Clicked(object sender, EventArgs e)
        {
            vSortMenu.IsVisible = true;
        }

        private void SortMenuItem_Clicked(object sender, EventArgs e)
        {
            vSortMenu.IsVisible = false;
        }
    }
}