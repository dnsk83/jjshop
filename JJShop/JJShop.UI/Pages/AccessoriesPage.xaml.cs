﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AccessoriesPage : MvxContentPage<AccessoriesViewModel>
	{
		public AccessoriesPage ()
		{
			InitializeComponent ();
		}

        private void MvxListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                ViewModel.GotoDemoCommand.Execute();
                ((MvxListView)sender).SelectedItem = null;
            }
        }
    }
}