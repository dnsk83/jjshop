﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DemoPage : MvxContentPage<DemoViewModel>
	{
		public DemoPage ()
		{
			InitializeComponent ();

            btnBack.Clicked += BtnBack_Clicked;

            if (Device.RuntimePlatform != Device.iOS)
            {
                var tgr = new TapGestureRecognizer();
                tgr.Tapped += (s, e) => { vMoreMenu.IsVisible = false; };
                vMoreMenu.GestureRecognizers.Add(tgr);

                btnTune.Clicked += BtnTune_Clicked_ShowMenu;
            }
            else
            {
                btnTune.Clicked += (s, e) =>
                {
                    DisplayActionSheet(null, "Cancel", null, "Select 1", "Select 2");
                };
            }
        }

        private void BtnBack_Clicked(object sender, EventArgs e)
        {
            ViewModel.NavigateBack();
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            vMoreMenu.IsVisible = false;
        }

        private void BtnTune_Clicked_ShowMenu(object sender, EventArgs e)
        {
            vMoreMenu.IsVisible = true;
        }

    }
}