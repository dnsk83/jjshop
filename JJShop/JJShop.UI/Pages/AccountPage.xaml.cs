﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(TabbedPosition.Tab, Title = "Account", Icon = "ic_account.png")]
    public partial class AccountPage : MvxContentPage<AccountViewModel>
	{
        public AccountPage ()
		{
			InitializeComponent ();
		}
    }
}