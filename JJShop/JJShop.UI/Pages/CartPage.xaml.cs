﻿using JJShop.UI.CustomViews;
using JJShop.Core.ViewModels;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static JJShop.Resources.Constants;
using MvvmCross.Forms.Presenters.Attributes;

namespace JJShop.UI.Pages
{
    [MvxTabbedPagePresentation(TabbedPosition.Tab, Title = "Cart", Icon = "ic_cart.png")]
    public partial class CartPage : MvxContentPage<CartViewModel>
	{
        public CartPage ()
		{
			InitializeComponent ();

            MessagingCenter.Subscribe<CartItemView>(this, Strings.ImTapped, OnItemTappedMessageReceived);
        }

        private void OnItemTappedMessageReceived(CartItemView sender)
        {
            lvItems.SelectedItem = sender;
        }


        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;

			if (Device.RuntimePlatform == Device.iOS)
			{
				var selectedItem = e.SelectedItem as CartItemViewModel;
                DisplayAlert(selectedItem.Name, selectedItem.Details, "Ok");
			}
			else
			{
				var selectedItem = e.SelectedItem as CartItemView;
                DisplayAlert(selectedItem.Name, selectedItem.Details, "Ok");
			}

            ((ListView)sender).SelectedItem = null;
        }
    }
}