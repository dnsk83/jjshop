﻿using JJShop.Core.ViewModels;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JJShop.UI.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(TabbedPosition.Root)]
    public partial class RootPage : MvxTabbedPage<RootViewModel>
	{
        private bool _firstTime = true;

        private Stack<Page> _TabNavigationStack;
        private Page _PreviousTab;
        private bool _IsNavigatingBack;

        public RootPage ()
		{
			InitializeComponent ();

            _TabNavigationStack = new Stack<Page>();

            _PreviousTab = CurrentPage;
            CurrentPageChanged += MainPage_CurrentPageChanged;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_firstTime)
            {
                ViewModel.ShowInitialViewModelsCommand.ExecuteAsync(null);
                _firstTime = false;
            }
        }

        private void AddTab(Page page, string title, FileImageSource icon)
        {
            var tab = page;
            tab.Title = title;
            tab.Icon = icon;
            Children.Add(tab);
        }

        private void MainPage_CurrentPageChanged(object sender, System.EventArgs e)
        {
            if (!_IsNavigatingBack)
            {
                _TabNavigationStack.Push(_PreviousTab);
            }
            else
            {
                _IsNavigatingBack = false;
            }
            _PreviousTab = CurrentPage;
        }

        protected override bool OnBackButtonPressed()
        {
            if (_TabNavigationStack.Count > 0)
            {
                _IsNavigatingBack = true;
                var tabNavigateTo = _TabNavigationStack.Pop();
                CurrentPage = tabNavigateTo;
                return true;
            }
            else
            {
                return base.OnBackButtonPressed();
            }
        }
    }
}