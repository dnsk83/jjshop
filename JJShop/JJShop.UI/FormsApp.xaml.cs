﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace JJShop.UI
{
    public partial class FormsApp : Application
    {
        public FormsApp()
        {
            InitializeComponent();
            InitializeDevice();
        }

        private void InitializeDevice()
        {
            switch (Xamarin.Forms.Device.RuntimePlatform)
            {
                case (Xamarin.Forms.Device.iOS):
                    JJShop.Core.Services.Device.RuntimePlatform = JJShop.Core.Services.Device.iOS;
                    break;
                case (Xamarin.Forms.Device.Android):
                    JJShop.Core.Services.Device.RuntimePlatform = JJShop.Core.Services.Device.Android;
                    break;
                default:
                    JJShop.Core.Services.Device.RuntimePlatform = JJShop.Core.Services.Device.Unknown;
                    break;
            }
        }
    }
}
