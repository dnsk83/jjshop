﻿using System;
using System.ComponentModel;
using CoreGraphics;
using JJShop.UI.CustomViews;
using JJShop.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ListsListItemView), typeof(ListsListItemViewRenderer))]
namespace JJShop.iOS.Renderers
{
    public class ListsListItemViewRenderer : ViewRenderer<ListsListItemView, UIView>
    {

        public UIPanGestureRecognizer panGesture;

        protected override void OnElementChanged(ElementChangedEventArgs<ListsListItemView> e)
        {
            base.OnElementChanged(e);

            var control = (ListsListItemView)Element;
            panGesture = new UIPanGestureRecognizer(() =>
            {

                if (control != null)
                {
                    CGPoint Frame = panGesture.TranslationInView(this);

                    var _gestureX = Frame.X;
                    var _gestureY = Frame.Y;

                    var onHorizontalSwipe = Math.Abs(_gestureX) > Math.Abs(_gestureY);
                    if (onHorizontalSwipe)
                    {
                        var onSwipeToLeft = _gestureX < 0;
                        if (onSwipeToLeft)
                        {
                            control.Swipe();
                            control.NotifyAboutSwipe();
                        }
                        else
                        {
                            control.Unswipe();
                        }
                    }
                }
            });

            panGesture.Delegate = new PangestureDelegate(this);
            this.AddGestureRecognizer(panGesture);
        }
    }

    public class PangestureDelegate : UIGestureRecognizerDelegate
    {
        ListsListItemViewRenderer _listsListItemViewRenderer;
        public PangestureDelegate(ListsListItemViewRenderer listsListItemViewRenderer)
        {
            _listsListItemViewRenderer = listsListItemViewRenderer;
        }

        public override bool ShouldRecognizeSimultaneously(UIGestureRecognizer gestureRecognizer, UIGestureRecognizer otherGestureRecognizer)
        {
            CGPoint Frame = ((UIPanGestureRecognizer)gestureRecognizer).TranslationInView(_listsListItemViewRenderer);

            var _gestureX = Frame.X;
            var _gestureY = Frame.Y;

            var onVerticalSwipe = Math.Abs(_gestureX) > Math.Abs(_gestureY);
            if (onVerticalSwipe)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


    }
}
