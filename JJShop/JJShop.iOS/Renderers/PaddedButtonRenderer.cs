﻿using System;
using JJShop.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Button), typeof(PaddedButtonRenderer))]
namespace JJShop.iOS.Renderers
{
    public class PaddedButtonRenderer : ButtonRenderer
    {
        public PaddedButtonRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            var element = this.Element as Button;
            if (element != null)
            {
                this.Control.ContentEdgeInsets = new UIEdgeInsets(0, 8, 0, 8);
            }
        }
    }
}
